package com.sample.sdk_integration;


import android.Manifest;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.URLUtil;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.adjust.sdk.Adjust;
import com.sample.sdk_integration.utilities.Utility;
import com.ma.paymentsdk.objects.MA_Constants;
import com.ma.paymentsdk.objects.MA_Dialogs;
import com.ma.paymentsdk.utilities.Logcat;
import com.ma.paymentsdk.utilities.MA_Utility;

public class SampleFragment extends Fragment {


    private View mView;
    private Context mContext;
    public static final String tag = "SampleFragment";

    private WebView webView;
    private Dialog dialog;
    private boolean started = true;
    private LinearLayout mLayoutError;
    private TextView mTextViewMessage;

    private final int REQUEST_WRITE_EXTERNAL_STORAGE = 9877;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_sample, container, false);
        mContext = getActivity();
        initWebView();
        return mView;
    }

    public void initWebView() {
        webView = (WebView) mView.findViewById(R.id.webview_portal);
        mLayoutError = (LinearLayout)mView.findViewById(R.id.layout_error) ;
        mTextViewMessage = (TextView)mView.findViewById(R.id.textview_message);


        CookieManager.getInstance().setAcceptCookie(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.setWebChromeClient(new WebChromeClient());



        if (MA_Utility.isOnline(mContext)) {

            mLayoutError.setVisibility(View.GONE);
            webView.setVisibility(View.VISIBLE);
            webView.postUrl(MA_Constants.CONTENT_REDIRECTION_URL, MA_Dialogs.getRedirectionUrl(mContext));

            webView.setWebViewClient(new WebViewController());

            webView.setDownloadListener(new DownloadListener() {
                public void onDownloadStart(String url, String userAgent,
                                            String contentDisposition, String mimeType,
                                            long contentLength) {
                    if(ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
                        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));

                        request.setMimeType(mimeType);
                        //------------------------COOKIE!!------------------------
                        String cookies = CookieManager.getInstance().getCookie(url);
                        request.addRequestHeader("cookie", cookies);
                        //------------------------COOKIE!!------------------------
                        request.addRequestHeader("User-Agent", userAgent);
                        request.setDescription("Downloading file...");
                        request.setTitle(URLUtil.guessFileName(url, contentDisposition, mimeType));

                        request.allowScanningByMediaScanner();
                        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, URLUtil.guessFileName(url, contentDisposition, mimeType));
                        DownloadManager dm = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
                        dm.enqueue(request);

                    }else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_EXTERNAL_STORAGE);
                        }
                    }

                }
            });


    } else {
        mLayoutError.setVisibility(View.VISIBLE);
        webView.setVisibility(View.GONE);
    }

    }

    @Override
    public void onResume() {
        Adjust.onResume();
        super.onResume();
        webView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        Adjust.onPause();
        webView.onPause();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        webView.loadUrl("about:blank");
    }

    public class WebViewController extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            if (started) {
                dialog = Utility.createDialog(mContext);
                dialog.show();
                started = false;
            }
        }

        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
            if (!started) {
                try {
                    Thread sthread = new Thread() {
                        @Override
                        public void run() {
                            try {
                                int waited = 0;
                                while (waited <= 300) {
                                    sleep(100);
                                    waited += 100;
                                }
                            } catch (InterruptedException e) {

                            } finally {

                                try {
                                    getActivity().runOnUiThread(new Runnable() {
                                        public void run() {
                                            Utility.removeDialog(dialog);
                                        }
                                    });
                                } catch (Exception e) {

                                }

                            }
                        }
                    };
                    sthread.start();
                } catch (Exception e) {
                    Utility.removeDialog(dialog);
                    Logcat.e(tag, e.getMessage());
                }
            }
        }

    }

}