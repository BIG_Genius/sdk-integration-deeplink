package com.sample.sdk_integration;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.adjust.sdk.Adjust;
import com.ma.paymentsdk.MobiboxConfig;
import com.sample.sdk_integration.utilities.Utility;
import com.ma.paymentsdk.MA_BillingActivity;
import com.ma.paymentsdk.MA_Lookup;
import com.ma.paymentsdk.interfaces.MA_OnLookUp;
import com.ma.paymentsdk.objects.MA_Billing;
import com.ma.paymentsdk.objects.MA_Constants;
import com.ma.paymentsdk.utilities.Logcat;
import com.ma.paymentsdk.utilities.MA_PreferenceData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;


public class SplashSceenActivity extends AppCompatActivity implements MA_OnLookUp {

    public static final String tag = "SplashSceenActivity";
    private final int SDK_CODE = 11;
    MA_Lookup lookup;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_splashscreen);
            getSupportActionBar().hide();

        } catch (Exception e) {
            Logcat.e(tag, "home error " + e.toString());
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        Adjust.onResume();
        Logcat.e(tag, "onResume: Adjust.onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Adjust.onPause();
    }

    private void navigateSample() {
        Logcat.e(tag, "nav next called , shouldbilling="+Utility.isDeeplinkUser(SplashSceenActivity.this));
        if (Utility.isNetworkAvailable(SplashSceenActivity.this)) {

            if (MA_Billing.getBillingStatus(SplashSceenActivity.this)) {
                callLookupSample();
            } else if(Utility.isDeeplinkUser(SplashSceenActivity.this)) {
                Intent intent = new Intent(SplashSceenActivity.this, MA_BillingActivity.class);
                startActivityForResult(intent, SDK_CODE);
            }else{
                gotoSample();
            }
        } else {
            gotoSample();
        }
    }

    private void callLookupSample() {
        lookup = new MA_Lookup(SplashSceenActivity.this);
        lookup.LookUpSubscriber(SplashSceenActivity.this);
    }

    private void gotoSample() {

        Intent browserIntent = new Intent(SplashSceenActivity.this, HomeActivity.class);
        startActivity(browserIntent);
        finish();
    }

    public void lookupResult(int status, String Description, Boolean isFreeTrial, String subscriptionDate, String expiryDate) {

        Adjust.onPause();
        Logcat.e(tag, "lookupResult: Adjust.onPause()");

        Logcat.e(tag, "User status: " + status + " description: " + Description);
        if (MA_Billing.isAppNeedsUpdate(SplashSceenActivity.this)) {
            finish();
            return;
        }
        try {
            //Status values:
            //0 for active users
            //1 for none active users
            //2 for deleted users
            //3 user not found
            if (status == 2 || status == 3) {
                Intent intent = new Intent(SplashSceenActivity.this,
                        MA_BillingActivity.class);
                intent.putExtra(MA_Constants.FROM_WHERE,
                        MA_Constants.InsideApp);
                startActivityForResult(intent, SDK_CODE);
            } else if (status == 1) {
                showAlertDialog(getString(R.string.recharge_phone));
            } else {
                gotoSample();
            }
        } catch (Exception e) {
            gotoSample();
        }
    }



    private void showAlertDialog(String message) {

        try {
            AlertDialog.Builder bld = new AlertDialog.Builder(SplashSceenActivity.this, R.style.MyAlertDialogStyle);
            bld.setCancelable(false);
            bld.setMessage(message);
            bld.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    gotoSample();
                }
            });
            bld.create().show();
        } catch (Exception e) {

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logcat.e(tag, "onActivityResult");

        Adjust.onPause();
        Logcat.e(tag, "onActivityResult: Adjust.onPause() : " + requestCode + " resultCode: " + resultCode);
        if (requestCode == SDK_CODE) {
            if (MA_Billing.isAppNeedsUpdate(SplashSceenActivity.this)) {
                finish();
                return;
            }
            gotoSample();

        } else if (requestCode == MA_Lookup.RC_READ) {
            Logcat.e(tag, "onActivityResult: requestCode == .RC_READ");
            if (lookup != null) {
                lookup.onActivityResult(requestCode, resultCode, data);
            }
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        // Branch init
        Branch.getInstance().initSession(new Branch.BranchReferralInitListener() {
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {

                    if (error == null) {

                            Log.e("BRANCH SDK", referringParams.toString());
                            // latest
                            JSONObject sessionParams = Branch.getInstance().getLatestReferringParams();
                            Log.e("sessions params :",sessionParams.toString());

                            // first
                            JSONObject installParams = Branch.getInstance().getFirstReferringParams();
                            Log.e("install params :",installParams.toString());
                            processDeepLinkParams(sessionParams);

                    } else {
                        Log.e("BRANCH SDK", error.getMessage());
                    }
                    // Start your task from here
                    navigateSample();

            }
        }, this.getIntent().getData(), this);

    }

    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }

    public void processDeepLinkParams(JSONObject deeplinkObj){
        try {
            if (deeplinkObj.has("OpenPage") && deeplinkObj.get("OpenPage").toString().equals("BActivity")) {
                Utility.setDeeplinkUser(this,true);
                MA_PreferenceData.setStringPref(MA_PreferenceData.KEY_DEEPLINK_REFERRER,
                        this, deeplinkObj.toString().replace("\\",""));
                String language = Locale.getDefault().toString();
                final boolean fireAd = Utility.getFireAD(deeplinkObj);
                Log.e("jprocessDeepLinkParams", "fireAd: "+fireAd);
                MobiboxConfig config = new MobiboxConfig(this, false, language,fireAd);

            }


        }catch (JSONException e){
            Log.e("json problem",e.toString());
        }

    }


}