package com.sample.sdk_integration.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.ma.paymentsdk.objects.MA_Billing;
import com.sample.sdk_integration.HomeActivity;
import com.sample.sdk_integration.R;
import com.sample.sdk_integration.interfaces.UpgradeToPremium;
import com.sample.sdk_integration.object.DrawerMenu;
import com.sample.sdk_integration.utilities.Utility;
import com.ma.paymentsdk.utilities.Logcat;

import java.util.ArrayList;

public class DrawerAdapter extends BaseAdapter {

	private static final String TAG = "DrawerAdapter";
	private Context context;
	private ArrayList<DrawerMenu> mMenus;
	private LayoutInflater mInflater;
	private UpgradeToPremium mUpgradeToPremium;

	public DrawerAdapter(Context context, UpgradeToPremium upgradeToPremium, ArrayList<DrawerMenu> menus) {
		super();
		this.context = context;
		this.mMenus = menus;
		this.mUpgradeToPremium = upgradeToPremium;
		this.mInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return mMenus.size();
	}


	@Override
	public Object getItem(int position) {
		return mMenus.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}


	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.list_item_menu, null);
			holder = new ViewHolder();
		try {
			holder.textViewItem = (TextView) convertView.findViewById(R.id.text_title);
			holder.iconItem = (ImageView) convertView
					.findViewById(R.id.image_icon);
			holder.layoutMain = (LinearLayout) convertView
					.findViewById(R.id.layout_drawer);
			holder.layoutUpgrade = (LinearLayout)convertView.findViewById(R.id.layout_upgrade);
			holder.layoutMenu = (RelativeLayout)convertView.findViewById(R.id.relativeData);
			holder.buttonPremiumFeature = (Button)convertView.findViewById(R.id.button_premium_features);
		}catch (Exception e){
			Logcat.e(TAG, "getView Exception1 : "+e.toString());
		}

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		try {

			holder.textViewItem.setText("" + mMenus.get(position).getTitle());
			holder.iconItem.setImageResource(mMenus.get(position).getIconResource());

			if (HomeActivity.selectedMenuPosition == position){
				holder.iconItem.setColorFilter(ContextCompat.getColor(context,R.color.colorPrimary));
				holder.textViewItem.setTextColor(context.getResources().getColor(R.color.colorPrimary));
			}else{
				holder.iconItem.setColorFilter(ContextCompat.getColor(context,R.color.gray));
				holder.textViewItem.setTextColor(context.getResources().getColor(R.color.gray));
			}
		}catch (Exception e){
			Logcat.e(TAG, "getView Exception2 : "+e.toString());
		}

		try {

			final int size = mMenus.size()-1;


			if (MA_Billing.getBillingStatus(context)) {
				if (position == size) {
					holder.layoutUpgrade.setVisibility(View.GONE);
					holder.layoutMenu.setVisibility(View.GONE);
				} else {
					holder.layoutUpgrade.setVisibility(View.GONE);
					holder.layoutMenu.setVisibility(View.VISIBLE);
				}

			} else {

				if (position == size ) {
					if(Utility.isDeeplinkUser(context)) {
						holder.layoutUpgrade.setVisibility(View.VISIBLE);
						holder.layoutMenu.setVisibility(View.GONE);
					}else{
						holder.layoutUpgrade.setVisibility(View.GONE);
						holder.layoutMenu.setVisibility(View.GONE);
					}
				} else {
					holder.layoutUpgrade.setVisibility(View.GONE);
					holder.layoutMenu.setVisibility(View.VISIBLE);
				}
			}


		}catch (Exception e){

		}

		try {
			holder.buttonPremiumFeature.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mUpgradeToPremium.clickOnUpgradeToPremium();

				}
			});
		}catch (Exception e){
			Logcat.e(TAG, "buttonPremiumFeature Exception: " + e.toString());
		}
		//Log.e(TAG, "buttonPremiumFeature UPGRADE_TO_PREMIUM: ");


		return convertView;
	}


	public class ViewHolder {
		TextView textViewItem;
		ImageView iconItem;
		RelativeLayout layoutMenu;
		LinearLayout layoutMain,layoutUpgrade;
		Button buttonPremiumFeature;


	}

}
