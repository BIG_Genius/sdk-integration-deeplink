package com.sample.sdk_integration.object;

public class DrawerMenu {
	
	public static final int HOME = 1;
	public static final int FLASH_SCREEN = 2;
	public static final int FLASH_CALL = 3;
	public static final int FLASH_KEYBOARD = 4;
	public static final int UPGRADE_TO_PREMIUM = 22;


	private int type;
	private String title;
	private int iconResource;
	
	
	public DrawerMenu(){
		super();
	}
	
	
	public DrawerMenu(int type, String title){
		this.type = type;
		this.title = title;
	}
	
	
	public DrawerMenu(int type, String title, int iconResource){
		this.type = type;
		this.title = title;
		this.iconResource = iconResource;
	}
	
	
	public int getType() {
		return type;
	}
	
	
	public void setType(int type) {
		this.type = type;
	}
	
	
	public String getTitle() {
		return title;
	}
	
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	
	public int getIconResource() {
		return iconResource;
	}
	
	
	public void setIconResource(int iconResource) {
		this.iconResource = iconResource;
	}
	
	
	
}
