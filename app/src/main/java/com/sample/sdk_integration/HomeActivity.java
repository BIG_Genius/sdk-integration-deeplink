package com.sample.sdk_integration;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.adjust.sdk.Adjust;
import com.sample.sdk_integration.adapter.DrawerAdapter;
import com.sample.sdk_integration.interfaces.UpgradeToPremium;
import com.sample.sdk_integration.object.DrawerMenu;
import com.ma.flashsdk.GifProvider.fragments.FlashCallFragment;
import com.ma.flashsdk.GifProvider.fragments.FlashKeyboardFragment;
import com.ma.flashsdk.GifProvider.fragments.FlashOnScreenFragment;
import com.ma.paymentsdk.MA_BillingActivity;
import com.ma.paymentsdk.objects.MA_Billing;
import com.ma.paymentsdk.objects.MA_Constants;
import com.ma.paymentsdk.utilities.Logcat;
import com.sample.sdk_integration.utilities.Utility;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity implements UpgradeToPremium{

    private static final String tag = "HomeActivity";
    private Context mContext;
    public static DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    public static DrawerAdapter mAdapter;
    private ArrayList<DrawerMenu> mMenus;
    public static ListView mListView;
    private int selectedMenu = 0;
    public static String mActionBarTitle = "";
    public static int selectedMenuPosition = 0;

    public static final int SDK_CODE = 11;

    public static UpgradeToPremium sUpgradeToPremium;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mContext = HomeActivity.this;
        sUpgradeToPremium = this;
        setActionBarTitle(getResources().getString(R.string.app_name), true);
        setupDrawerMenu(savedInstanceState);

    }

    public void setupDrawerMenu(Bundle savedInstanceState) {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        toggle = new ActionBarDrawerToggle(
                this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                setActionBarTitle(mActionBarTitle, true);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                setActionBarTitle(getResources().getString(R.string.app_name),true);
            }
        };

        drawer.setDrawerListener(toggle);

        toggle.setDrawerIndicatorEnabled(true);
        toggle.syncState();
        setDrawerMenu();
    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(mListView)) {
            drawer.closeDrawer(mListView);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Adjust.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Adjust.onPause();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

    }


    public void setDrawerMenu() {
        mListView = (ListView) findViewById(R.id.lst_menu_items);
        mMenus = new ArrayList<DrawerMenu>();
        mMenus.add(new DrawerMenu(DrawerMenu.HOME, getResources().getString(R.string.home),
                R.drawable.ic_home));
        mMenus.add(new DrawerMenu(DrawerMenu.FLASH_SCREEN, getResources().getString(R.string.flash_screen),
                R.drawable.ic_screen));
        mMenus.add(new DrawerMenu(DrawerMenu.FLASH_CALL, getResources().getString(R.string.flash_call),
                R.drawable.ic_call));
        mMenus.add(new DrawerMenu(DrawerMenu.FLASH_KEYBOARD, getResources().getString(R.string.flash_keyboard),
                R.drawable.ic_keyboard));
        mMenus.add(new DrawerMenu(DrawerMenu.UPGRADE_TO_PREMIUM, getResources().getString(R.string.upgrade_to_premium),
                R.drawable.ic_btn_premium));

        mAdapter = new DrawerAdapter(HomeActivity.this, this,mMenus);
        mListView.setAdapter(mAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent,final View view,final int position,final long id) {

                   DrawerMenu menu = mMenus.get(position);

                  if(menu.getType() == DrawerMenu.UPGRADE_TO_PREMIUM){
                      Logcat.e("toggleDrawer", "DrawerMenu UPGRADE_TO_PREMIUM");

                  }else {
                      navigateToSample(position, true);
                  }

            }
        });

        navigateToSample(0, true);
        toggleDrawer();

    }

    private void toggleDrawer() {

        try {
            if (!drawer.isDrawerOpen(mListView)) {
                drawer.openDrawer(mListView);
            } else {
                drawer.closeDrawer(mListView);
            }

        } catch (Exception e) {
            Logcat.e("toggleDrawer", "toggleDrawer Exception: " + e.toString());
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        try {
            //  Logcat.e("id", "id: " + id);
            if (id == 16908332) {
                toggleDrawer();
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    public void refreshWebViewSample(){
        try {
            if (selectedMenuPosition == 0){
                navigateToSample(selectedMenuPosition, true);
            }
        }catch (Exception e){

        }
    }

    private void navigateToSample(final int position, final boolean toggle) {
        DrawerMenu menu = mMenus.get(position);
        if (toggle) {
            toggleDrawer();
        }
        selectedMenuPosition = position;

        FragmentManager fragmentManager = getSupportFragmentManager();

        switch (menu.getType()) {
            case DrawerMenu.HOME:
                mActionBarTitle = getResources().getString(R.string.home);
                fragmentManager.beginTransaction().replace(R.id.container, new SampleFragment())
                        .commit();
                setActionBarTitle(getResources().getString(R.string.home), true);
                break;
            case DrawerMenu.FLASH_SCREEN:
                mActionBarTitle = getResources().getString(R.string.flash_screen);
                fragmentManager.beginTransaction().replace(R.id.container, new FlashOnScreenFragment())
                        .commit();
                setActionBarTitle(getResources().getString(R.string.flash_screen), true);
                break;
            case DrawerMenu.FLASH_KEYBOARD:
                mActionBarTitle = getResources().getString(R.string.flash_keyboard);
                fragmentManager.beginTransaction().replace(R.id.container, new FlashKeyboardFragment())
                        .commit();
                setActionBarTitle(getResources().getString(R.string.flash_keyboard), true);
                break;
            case DrawerMenu.FLASH_CALL:
                mActionBarTitle = getResources().getString(R.string.flash_call);
                fragmentManager.beginTransaction().replace(R.id.container, new FlashCallFragment())
                        .commit();
                setActionBarTitle(getResources().getString(R.string.flash_call), true);
                break;
        }

       // setActionBarTitle(mActionBarTitle);
        mAdapter.notifyDataSetChanged();
    }

    public void setActionBarTitle(String title, boolean showHome) {
        final ActionBar abar = getSupportActionBar();

        if (title == null || title.equals("")){
            title = ""+getResources().getString(R.string.app_name);
        }
        View viewActionBar = getLayoutInflater().inflate(
                R.layout.layout_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT, Gravity.CENTER);
        TextView textviewTitle = (TextView) viewActionBar
                .findViewById(R.id.actionbar_textview);
        textviewTitle.setText(title);
        abar.setCustomView(viewActionBar, params);
        abar.setDisplayShowCustomEnabled(true);
        abar.setDisplayShowTitleEnabled(false);
        if (showHome) {
            abar.setDisplayHomeAsUpEnabled(true);
            abar.setHomeButtonEnabled(true);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Adjust.onPause();
        if (requestCode == SDK_CODE) {
            if (resultCode == RESULT_OK) {
                if (MA_Billing.getBillingStatus(HomeActivity.this)) {
                    mAdapter.notifyDataSetChanged();
                    if (selectedMenuPosition == 0){
                        navigateToSample(selectedMenuPosition, true);
                    }
                }
            }

        }
    }

    @Override
    public void clickOnUpgradeToPremium() {
        if (Utility.isDeeplinkUser(HomeActivity.this)){
            Intent intent = new Intent(HomeActivity.this, MA_BillingActivity.class);
            intent.putExtra(MA_Constants.FROM_WHERE, MA_Constants.InsideApp);
            startActivityForResult(intent, SDK_CODE);
        }
    }
}
