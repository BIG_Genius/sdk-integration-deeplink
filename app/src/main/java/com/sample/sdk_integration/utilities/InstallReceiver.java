package com.sample.sdk_integration.utilities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.google.android.gms.analytics.CampaignTrackingReceiver;
import com.ma.paymentsdk.broadcast.MA_CampaignTrackingReceiver;


public class InstallReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        // Google Analytics
        new CampaignTrackingReceiver().onReceive(context, intent);

        new MA_CampaignTrackingReceiver().onReceive(context, intent);
    }
}
