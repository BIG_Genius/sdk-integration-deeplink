package com.sample.sdk_integration.utilities;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import java.lang.reflect.Field;

import io.branch.referral.Branch;

public class SampleApplication extends Application {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // Branch logging for debugging
        Branch.enableLogging();

        // Branch object initialization
        Branch.getAutoInstance(this).enableFacebookAppLinkCheck();


    }

}